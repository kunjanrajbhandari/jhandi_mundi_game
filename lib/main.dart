import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

enum Dice{
  CLUB, KING, HEART, DIAMOND, FLAG, SPADE
}

class MyApp extends StatefulWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Dice? selectedDice;

  List<Dice> results =[];
  void generateResult(){
    results = [];
    for(int i=0; i<6; i++){
      Random random = Random();
      int getValue = random.nextInt(6)+1;
      
      Dice result = getDiceFromNum(getValue);
      results.add(result);

      print(result);
    }
  }

  Dice getDiceFromNum(int value){
    switch(value){
      case 1:
      return Dice.SPADE;

      case 2:
      return Dice.KING;

      case 3:
      return Dice.CLUB;

      case 4:
      return Dice.DIAMOND;

      case 5:
      return Dice.FLAG;

      case 6:
      return Dice.HEART;
      
      default:
      return Dice.CLUB;
    }
  }
  String assetFactory(Dice dice){

    switch(dice){
      case Dice.CLUB:
      return 'assets/clubs.png';

      case Dice.DIAMOND:
      return 'assets/diamond.png';

      case Dice.FLAG:
      return 'assets/flag.png';

      case Dice.KING:
      return 'assets/kings.png';

      case Dice.SPADE:
      return 'assets/spade.png';

      case Dice.HEART:
      return 'assets/heart.png';

      default: 
      return 'assets/clubs.png';
    }
  }

  String checkWin(){
    for(int i=0; i<results.length; i++){
      if(selectedDice ==results[i])
      return 'Win';
    }
    return 'Loose';
  }
  List<Widget> getResultsInWidget(){
    List<Widget> displayResult = [];
    for (int i=0; i<results.length; i++){
      displayResult.add(
        Expanded(child: Image(image: AssetImage(assetFactory(results[i]))))
      );
    }
    return displayResult;
  }
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Jhandi Munda"),
        ),
        body: Column(
          children: [
            Container(
              margin: EdgeInsets.all(20),
              child: Column(
                children: [
                    Row(
                      children: [
                        
                        Expanded(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: selectedDice == Dice.SPADE ? Colors.amber : Colors.white,
                            ),
                            onPressed: (){
                              setState(() {
                                  selectedDice = Dice.SPADE;
                              });
                            
                            },
                            child: Image(image: AssetImage('assets/spade.png'))
                          )
                        ),

                        Expanded(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: selectedDice == Dice.KING ? Colors.amber : Colors.white,
                            ),
                            onPressed: (){
                              setState(() {
                                  selectedDice = Dice.KING;
                              });
                            },
                            child: Image(image: AssetImage('assets/kings.png'))
                          )
                        ),

                        Expanded(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: selectedDice == Dice.CLUB ? Colors.amber : Colors.white,
                            ),
                            onPressed: (){
                              setState(() {
                                  selectedDice = Dice.CLUB;
                              });
                            },
                            child: Image(image: AssetImage('assets/clubs.png'))
                          )
                        ),                        
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: selectedDice == Dice.DIAMOND ? Colors.amber : Colors.white,
                            ),
                            onPressed: (){
                              setState(() {
                                  selectedDice = Dice.DIAMOND;
                              });
                            },
                            child: Image(image: AssetImage('assets/diamond.png'))
                          )
                        ),

                        Expanded(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              
                              primary: selectedDice == Dice.FLAG ? Colors.amber : Colors.white,
                            ),
                            onPressed: (){
                              setState(() {
                                  selectedDice = Dice.FLAG;
                              });
                            },
                            child: Image(image: AssetImage('assets/flag.png'))
                          )
                        ),

                        Expanded(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: selectedDice == Dice.HEART ? Colors.amber : Colors.white,
                            ),
                            onPressed: (){
                              setState(() {
                                  selectedDice = Dice.HEART;
                              });
                            },
                            child: Image(image: AssetImage('assets/heart.png'))
                          )
                        ),
                      ],
                    )
                ],
              ),
            ),
            Visibility(
              visible: selectedDice!=null ? true : false,
              child: ElevatedButton(
                onPressed: (){
                  setState(() {
                    generateResult();
                  });
                  
                },
                child: Text("Dice Roll", style: TextStyle(fontSize: 20.0),),
              ),
            ),
            Row(
              children: getResultsInWidget(),
            )
          ],
        ),
        bottomSheet: Visibility(
          visible: results.length>0 ? true : false,
          child: Container(
            height: 140.0,
            width: double.infinity,
            color: Colors.red,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(padding: EdgeInsets.only(top: 20.0)),
                Text(checkWin(), style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold, color: Colors.white),),
                Padding(padding: EdgeInsets.only(top: 20.0)),
                ElevatedButton(
                  onPressed: (){
                    setState(() {
                      selectedDice = null;
                      results=[];
                    });
                  },
                  child: Text("Restart", style: TextStyle(fontSize: 20.0),),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}